﻿using System;

namespace Microservicio.Venta.Domain
{
    public class Sale
    {
        public Sale()
        {
            Date = DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string CustomerName { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
