﻿using Microservicio.Venta.Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace Microservicio.Venta.Persistance
{
    public class SaleDbContext : DbContext
    {
        public SaleDbContext(DbContextOptions<SaleDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sale>()
                .ToTable("Sales")
                .HasKey("Id");
        }

        public DbSet<Sale> Sales { get; set; }
    }
}
